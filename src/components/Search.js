import { useState } from "react";

import { SEARCH_API } from "../App";
import { FEATURED_API } from "../App";

export const Search = ({ fetchMoviesData }) => {
  const [searchValue, setSearchValue] = useState("");

  const handleOnChange = (e) => {
    setSearchValue(e.target.value);
  };

  const handleOnSubmit = async (e) => {
    e.preventDefault();

    if (searchValue) {
      fetchMoviesData(`${SEARCH_API}${searchValue}`);
      setSearchValue("");
    }

    if (searchValue === "") {
      fetchMoviesData(FEATURED_API);
    }
  };

  return (
    <header>
      <form onSubmit={handleOnSubmit}>
        <input
          className="search"
          type="search"
          placeholder="Search movie..."
          value={searchValue}
          onChange={handleOnChange}
        ></input>
      </form>
    </header>
  );
};
