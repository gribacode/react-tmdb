import React from "react";

import { IMAGES_API } from "../App";

const setVoteClass = (voteAverage) => {
  if (voteAverage >= 8) {
    return "green";
  } else if (voteAverage >= 6) {
    return "orange";
  } else {
    return "red";
  }
};

export const Movie = ({ title, poster_path, overview, vote_average }) => (
  <div className="movie-card">
    <img
      src={
        poster_path
          ? `${IMAGES_API}${poster_path}`
          : "https://images.unsplash.com/photo-1560109947-543149eceb16?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=435&q=80"
      }
      alt={title}
    />
    <div className="card-information">
      <h3>{title}</h3>
      <span className={`colors ${setVoteClass(vote_average)}`}>
        {vote_average}
      </span>
    </div>
    <div className="movie-overview">
      <h2>Overview:</h2>
      <p className={!overview && "red"}>
        {overview ? overview : "Overview is empty"}
      </p>
    </div>
  </div>
);
