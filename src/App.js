import React, { useEffect, useState } from "react";

import { Movie } from "./components/Movie";
import { Empty } from "./components/Empty";
import { Search } from "./components/Search";

export const FEATURED_API =
  "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=e4b4120c4930c27a18bf63e5d6bf4b73&page=1";

export const IMAGES_API = "https://image.tmdb.org/t/p/w500/";

export const SEARCH_API =
  "https://api.themoviedb.org/3/search/movie?&api_key=e4b4120c4930c27a18bf63e5d6bf4b73&query=";

function App() {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    fetchMoviesData(FEATURED_API);
  }, []);

  const fetchMoviesData = async (API) => {
    await fetch(API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.total_results > 0) {
          setMovies(data.results);
        } else {
          setMovies([]);
        }
      });
  };

  return (
    <>
      <Search fetchMoviesData={fetchMoviesData} />
      <div className="movie-container">
        {movies.length ? (
          movies.map((movieCard) => <Movie key={movieCard.id} {...movieCard} />)
        ) : (
          <Empty />
        )}
      </div>
    </>
  );
}

export default App;
